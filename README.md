# Chess console
**Description:**

Completed all items from the task.

Used Linux, Qt 5.14.2 GCC 64-bit.

For unit-test include Test_Validator to project (and all "for test"). File with result test "testing.log".

File for play on Linux in linux/chess

**Task:**

Implement multiplayer chess game so that two players may sitting by one PC. 

Required functionality:


    1. The application shows chess board in console.
    2. Players may interact with the application using command line instructions. For example:
        a. Start/restart game
        b. Save game
        c. Load game
        d. Move
    3. The application must validate all moves (if a player wants to make a move that is not allowed by chess rules, the application must block such attempt and show corresponding notification)
    4. The application must correctly handle all chess rules (castling, check, checkmate, …)
    5. Testing: please implement unit tests for those classes which contain business logic

Technical requirements / limitations:

    1. Programming language: C++
    2. External libraries: STL only
    3. Platform: Linux and/or Windows