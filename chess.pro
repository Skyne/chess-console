TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt
#CONFIG += qt
#QT += testlib - for test

SOURCES += \
        game.cpp \
        main.cpp \
#        test_validator.cpp \
        validator.cpp

HEADERS += \
    figure.h \
    game.h \
#    test_validator.h \
    validator.h
