#ifndef FIGURE_H
#define FIGURE_H

#include <string>
#include <list>

struct Figure
{
    std::string name = "";
    int posX = 0;
    int posY = 0;
    std::list<std::string> underAttack = {};

    bool isEmpty()
    {
        return name.empty();
    };
    void clear()
    {
        name.clear();
    };
    std::string serialise()
    {
        return name + std::to_string(posX) + std::to_string(posY);
    };
};

#endif // FIGURE_H
