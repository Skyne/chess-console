#include "game.h"

using namespace std;

Game::Game()
{
    int j = 0;
    for (int i = 3; i <= 17; i += 2)
    {
        accordance[j][0] = { i, 4 };
        accordance[j][1] = { i, 7 };
        accordance[j][2] = { i, 10 };
        accordance[j][3] = { i, 13 };
        accordance[j][4] = { i, 16 };
        accordance[j][5] = { i, 19 };
        accordance[j][6] = { i, 22 };
        accordance[j][7] = { i, 25 };
        ++j;
    }
    for (int i = 0; i < 6; i++)
        checkCastling.push_back(false);
}

void Game::displayBoard()
{
    system("clear");

    cout << "For save game input <s>, load game <l>, restart game <r>, exit <e>" << endl << endl;

    string board[] = { "            Black  ",
                       "   |a |b |c |d |e |f |g |h |  ",
                       "  --------------------------- ",
                       " 1 |  |  |  |  |  |  |  |  | 1",
                       "  --------------------------- ",
                       " 2 |  |  |  |  |  |  |  |  | 2",
                       "  --------------------------- ",
                       " 3 |  |  |  |  |  |  |  |  | 3",
                       "  --------------------------- ",
                       " 4 |  |  |  |  |  |  |  |  | 4",
                       "  --------------------------- ",
                       " 5 |  |  |  |  |  |  |  |  | 5",
                       "  --------------------------- ",
                       " 6 |  |  |  |  |  |  |  |  | 6",
                       "  --------------------------- ",
                       " 7 |  |  |  |  |  |  |  |  | 7",
                       "  --------------------------- ",
                       " 8 |  |  |  |  |  |  |  |  | 8",
                       "  --------------------------- ",
                       "   |a |b |c |d |e |f |g |h |  ",
                       "            White  " };

    for (auto& it : mapBoard)
    {
        if (!it.second.isEmpty())
        {
            pair boardPos = accordance[it.second.posX][it.second.posY];
            board[boardPos.first][boardPos.second] = it.second.name[0];
            board[boardPos.first][boardPos.second + 1] = it.second.name[1];
        }
    }
    Validator::fillUnderAttack(mapBoard);
    for (const auto& tmp : board)
        cout << tmp << endl;
    string kingPos = Validator::checkCheck(mapBoard);
    if (kingPos != "")
    {
        if (Validator::checkMate(mapBoard, kingPos))
        {
            cout << "CHECKMATE!" << endl;
            cout << "Congratulations! " << (!turnWhite ? "White" : "Black") << " Win!" << endl;
            cout << "For restart game press any key, for exit press <e>..." << endl;
            string tmp = "";
            cin >> tmp;
            if (tmp == "e")
                closeGame();
            system("clear");
            startGame();
        }
        else
        {
            cout << "CHECK!" << endl;
        }
    }
    if (Validator::checkStalemate(mapBoard, turnWhite))
    {
        cout << "Stalemate!" << endl;
        cout << "For restart game press any key, for exit press <e>..." << endl;
        string tmp = "";
        cin >> tmp;
        if (tmp == "e")
            closeGame();
        system("clear");
        startGame();
    }
}

void Game::startGame()
{
    cout << "Welcome to the Chess" << endl;
    string input = "";
    bool ex = true;
    while (ex)
    {
        cout << "Please choose: N - new game, L - Load game -> ";
        cin >> input;
        if (input == "N" || input == "n")
        {
            initPosition();
            bool isWhite = false;
            cout << "first turn" << endl;
            randomChoose(isWhite);
            turnWhite = isWhite;
            ex = false;
            if (isWhite)
                cout << "White" << endl;
            else
                cout << "Black" << endl;
            sleep(0, 1);
        }
        else if (input == "L" || input == "l")
        {
            if (loadGame())
            {
                cout << "Game loaded" << endl;
                ex = false;
            }
            else
                cout << "Error read file, try again" << endl;
            sleep(0, 1);
            system("clear");
        }
        else
        {
            cout << "Incorrect input, try again press (a), exit press any another key -> ";
            cin >> input;
            cout << input << endl;
            if (input != "a" && input != "A")
            {
                exit(0);
            }
            system("clear");
        }
    }

    gameProcess();
}

void Game::initPosition()
{
    // black pawn start position BP
    mapBoard["a2"] = { "BP", 1, 0 };
    mapBoard["b2"] = { "BP", 1, 1 };
    mapBoard["c2"] = { "BP", 1, 2 };
    mapBoard["d2"] = { "BP", 1, 3 };
    mapBoard["e2"] = { "BP", 1, 4 };
    mapBoard["f2"] = { "BP", 1, 5 };
    mapBoard["g2"] = { "BP", 1, 6 };
    mapBoard["h2"] = { "BP", 1, 7 };

    // black figure start position
    mapBoard["a1"] = { "BR", 0, 0 };
    mapBoard["b1"] = { "BN", 0, 1 };
    mapBoard["c1"] = { "BB", 0, 2 };
    mapBoard["d1"] = { "BQ", 0, 3 };
    mapBoard["e1"] = { "BK", 0, 4 };
    mapBoard["f1"] = { "BB", 0, 5 };
    mapBoard["g1"] = { "BN", 0, 6 };
    mapBoard["h1"] = { "BR", 0, 7 };

    // white pawn start position WP
    mapBoard["a7"] = { "WP", 6, 0 };
    mapBoard["b7"] = { "WP", 6, 1 };
    mapBoard["c7"] = { "WP", 6, 2 };
    mapBoard["d7"] = { "WP", 6, 3 };
    mapBoard["e7"] = { "WP", 6, 4 };
    mapBoard["f7"] = { "WP", 6, 5 };
    mapBoard["g7"] = { "WP", 6, 6 };
    mapBoard["h7"] = { "WP", 6, 7 };

    // white figure start position
    mapBoard["a8"] = { "WR", 7, 0 };
    mapBoard["b8"] = { "WN", 7, 1 };
    mapBoard["c8"] = { "WB", 7, 2 };
    mapBoard["d8"] = { "WQ", 7, 3 };
    mapBoard["e8"] = { "WK", 7, 4 };
    mapBoard["f8"] = { "WB", 7, 5 };
    mapBoard["g8"] = { "WN", 7, 6 };
    mapBoard["h8"] = { "WR", 7, 7 };

    for (int i = 3; i < 7; i++)
    {
        mapBoard["a" + to_string(i)] = { "", i - 1, 0 };
        mapBoard["b" + to_string(i)] = { "", i - 1, 1 };
        mapBoard["c" + to_string(i)] = { "", i - 1, 2 };
        mapBoard["d" + to_string(i)] = { "", i - 1, 3 };
        mapBoard["e" + to_string(i)] = { "", i - 1, 4 };
        mapBoard["f" + to_string(i)] = { "", i - 1, 5 };
        mapBoard["g" + to_string(i)] = { "", i - 1, 6 };
        mapBoard["h" + to_string(i)] = { "", i - 1, 7 };
    }
}

void Game::randomChoose(bool& isWhite)
{
    long count = 1;
    srand(time(0));
    while (count < 3000)
    {
        sleep(count);
        isWhite = rand() % 2;
        count += 1;
        if (isWhite)
            printf("White\r");
        else
            printf("Black\r");
    }
}

void Game::gameProcess()
{
    bool ex = false;
    string input = "";
    while (!ex)
    {
        displayBoard();
        cout << "Now turn " << (turnWhite ? "White" : "Black") << endl;
        cout << "Please choose figure -> ";
        cin >> input;
        if (input.size() == 1)
            if (Validator::checkOption(input[0]))
                break;
        if (!Validator::checkInput(input))
        {
            sleep(0, 2);
            continue;
        }
        string nameFigure = mapBoard[input].name;
        if (!Validator::checkFigure(nameFigure, turnWhite))
        {
            sleep(0, 2);
            continue;
        }
        string figurePos = input;
        cout << "Please choose turn -> ";
        cin >> input;
        if (!Validator::checkInput(input))
        {
            sleep(0, 2);
            continue;
        }
        if (!Validator::checkTurn(figurePos, input, mapBoard, turnWhite, checkCastling))
        {
            cout << "Incorrect turn" << endl;
            sleep(0, 2);
            continue;
        }
        if (Validator::checkKingUnderAttack(mapBoard, turnWhite, figurePos, input))
        {
            cout << "King is in danger!" << endl;
            sleep(0, 2);
            continue;
        }
        Castling checkCast = Validator::checkCastling(mapBoard, figurePos, input, checkCastling);
        if (checkCast == Castling::CORRECT)
        {
            if (input == "c1")
            {
                if (Validator::checkKingUnderAttack(mapBoard, turnWhite, "e1", "c1"))
                {
                    cout << "Сastling is not possible!" << endl;
                    sleep(0, 2);
                    continue;
                }
                mapBoard["d1"].name = mapBoard["a1"].name;
                mapBoard["a1"].clear();
            }
            else if (input == "g1")
            {
                if (Validator::checkKingUnderAttack(mapBoard, turnWhite, "e1", "f1"))
                {
                    cout << "Castling is not possible" << endl;
                    sleep(0, 2);
                    continue;
                }
                mapBoard["f1"].name = mapBoard["h1"].name;
                mapBoard["h1"].clear();
            }
            else if (input == "c8")
            {
                if (Validator::checkKingUnderAttack(mapBoard, turnWhite, "e8", "d8"))
                {
                    cout << "Castling is not possible" << endl;
                    sleep(0, 2);
                    continue;
                }
                mapBoard["d8"].name = mapBoard["a8"].name;
                mapBoard["a8"].clear();
            }
            else if (input == "g8")
            {
                if (Validator::checkKingUnderAttack(mapBoard, turnWhite, "e8", "f8"))
                {
                    cout << "Castling is not possible" << endl;
                    sleep(0, 2);
                    continue;
                }
                mapBoard["f8"].name = mapBoard["h8"].name;
                mapBoard["h8"].clear();
            }
            cout << "Castling!" << endl;
            sleep(0, 2);
        }
        else if (checkCast == Castling::NOT_CORRECT)
        {
            cout << "Castling is not possible" << endl;
            sleep(0, 2);
            continue;
        }

        string tmp = mapBoard[figurePos].name;
        mapBoard[figurePos].clear();
        mapBoard[input].name = tmp;
        turnWhite = !turnWhite;
    }

    char option = input[0];
    switch (option)
    {
    case 'e': {
        closeGame();
        break;
    }
    case 's': {
        saveGame();
        gameProcess();
        break;
    }
    case 'l': {
        if (!loadGame())
        {
            cout << "Incorrect file" << endl;
            sleep(0, 2);
        }
        gameProcess();
        break;
    }
    case 'r': {
        system("clear");
        startGame();
        break;
    }
    }
}

void Game::sleep(long msec, long sec)
{
    long wait = msec * 1000;
    struct timespec tw = { sec, wait };
    struct timespec tr;
    nanosleep(&tw, &tr);
}

void Game::saveGame()
{
    string turn = turnWhite ? "W" : "B";
    string data = serialise() + turn + serializeCheckCast();
    cout << "Name of save? -> ";
    string input = "";
    cin >> input;
    std::ofstream fout;
    fout.open(input + ".s");
    fout << data;
    fout.close();
    cout << "Game saved" << endl;
    sleep(0, 1);
}

bool Game::loadGame()
{
    cout << "Input saved game name -> ";
    string input = "";
    cin >> input;

    std::ifstream file(input + ".s");
    if (!file.is_open())
        return false;

    map<string, Figure> tmpMap;
    file >> input;
    try
    {
        ulong in = 0;
        while (in < input.size() - 7)
        {
            string pos = input.substr(in, 2);
            string name = input.substr(in + 2, 2);
            int posX = input[in + 4] - '0';
            int posY = input[in + 5] - '0';
            Figure figure { name, posX, posY };
            tmpMap[pos] = figure;
            in += 6;
        }
        turnWhite = false;
        if (input[in] == 'W')
            turnWhite = true;
        ++in;
        for (int i = 0; i < 6; i++)
            checkCastling[i] = (input[in + i] == '1' ? true : false);
    } catch (...)
    {
        return false;
    }
    mapBoard.clear();
    mapBoard = tmpMap;
    fillMapEmpty(mapBoard);
    return true;
}

void Game::closeGame()
{
    system("clear");
    cout << "Thank you for playing" << endl;
    sleep(0, 2);
    exit(0);
}

string Game::serialise()
{
    string data = "";
    for (auto& tmp : mapBoard)
    {
        if (tmp.second.isEmpty())
            continue;
        data += tmp.first + tmp.second.serialise();
    }
    return data;
}

string Game::serializeCheckCast()
{
    string data = "";
    for (int i = 0; i < 6; i++)
        data += (checkCastling[i] == true ? "1" : "0");
    return data;
}

void Game::fillMapEmpty(std::map<string, Figure>& maps)
{
    for (int i = 0; i < 8; i++)
        for (int j = 1; j <= 8; j++)
        {
            char letter = Validator::convert(i);
            if (maps[(letter + to_string(j))].isEmpty())
                maps[(letter + to_string(j))] = { "", j - 1, i };
        }
}
