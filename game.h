#ifndef GAME_H
#define GAME_H
#include <iostream>
#include <map>
#include <fstream>
#include <vector>
#include "validator.h"

class Game
{
public:
    Game();
    void startGame();

private:
    void initPosition();
    void displayBoard();
    void randomChoose(bool &white);
    void gameProcess();
    void sleep(long msec, long sec = 0);
    void saveGame();
    bool loadGame();
    void closeGame();
    std::string serialise(); // for save file
    std::string serializeCheckCast();
    void fillMapEmpty(std::map<std::string, Figure> &maps); // fill after load

    std::vector<bool> checkCastling; // for check castling

    bool turnWhite = false; // who turn now
    std::map<std::string, Figure> mapBoard;
    std::pair<int, int> accordance[8][8] = {}; // for visual display
};

#endif // GAME_H
