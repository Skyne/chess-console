#include "test_validator.h"
using namespace std;

Test_Validator::Test_Validator(QObject* parent)
    : QObject(parent)
{
    for (int i = 0; i < 6; i++)
        cast.push_back(false);

    // black pawn start position BP
    mapBoard["a2"] = { "BP", 1, 0 };
    mapBoard["b2"] = { "BP", 1, 1 };
    mapBoard["c2"] = { "BP", 1, 2 };
    mapBoard["d2"] = { "BP", 1, 3 };
    mapBoard["e2"] = { "BP", 1, 4 };
    mapBoard["f2"] = { "BP", 1, 5 };
    mapBoard["g2"] = { "BP", 1, 6 };
    mapBoard["h2"] = { "BP", 1, 7 };

    // black figure start position
    mapBoard["a1"] = { "BR", 0, 0 };
    mapBoard["b1"] = { "BN", 0, 1 };
    mapBoard["c1"] = { "BB", 0, 2 };
    mapBoard["d1"] = { "BQ", 0, 3 };
    mapBoard["e1"] = { "BK", 0, 4 };
    mapBoard["f1"] = { "BB", 0, 5 };
    mapBoard["g1"] = { "BN", 0, 6 };
    mapBoard["h1"] = { "BR", 0, 7 };

    // white pawn start position WP
    mapBoard["a7"] = { "WP", 6, 0 };
    mapBoard["b7"] = { "WP", 6, 1 };
    mapBoard["c7"] = { "WP", 6, 2 };
    mapBoard["d7"] = { "WP", 6, 3 };
    mapBoard["e7"] = { "WP", 6, 4 };
    mapBoard["f7"] = { "WP", 6, 5 };
    mapBoard["g7"] = { "WP", 6, 6 };
    mapBoard["h7"] = { "WP", 6, 7 };

    // white figure start position
    mapBoard["a8"] = { "WR", 7, 0 };
    mapBoard["b8"] = { "WN", 7, 1 };
    mapBoard["c8"] = { "WB", 7, 2 };
    mapBoard["d8"] = { "WQ", 7, 3 };
    mapBoard["e8"] = { "WK", 7, 4 };
    mapBoard["f8"] = { "WB", 7, 5 };
    mapBoard["g8"] = { "WN", 7, 6 };
    mapBoard["h8"] = { "WR", 7, 7 };

    for (int i = 3; i < 7; i++)
    {
        mapBoard["a" + to_string(i)] = { "", i - 1, 0 };
        mapBoard["b" + to_string(i)] = { "", i - 1, 1 };
        mapBoard["c" + to_string(i)] = { "", i - 1, 2 };
        mapBoard["d" + to_string(i)] = { "", i - 1, 3 };
        mapBoard["e" + to_string(i)] = { "", i - 1, 4 };
        mapBoard["f" + to_string(i)] = { "", i - 1, 5 };
        mapBoard["g" + to_string(i)] = { "", i - 1, 6 };
        mapBoard["h" + to_string(i)] = { "", i - 1, 7 };
    }
}

void Test_Validator::checkOption()
{
    QCOMPARE(Validator::checkOption('e'), true);
    QCOMPARE(Validator::checkOption('l'), true);
    QCOMPARE(Validator::checkOption('r'), true);
    QCOMPARE(Validator::checkOption('s'), true);
    QCOMPARE(Validator::checkOption(' '), false);
    QCOMPARE(Validator::checkOption('1'), false);
    QCOMPARE(Validator::checkOption(']'), false);
    QCOMPARE(Validator::checkOption('`'), false);
}

void Test_Validator::checkInput()
{
    QCOMPARE(Validator::checkInput("a3"), true);
    QCOMPARE(Validator::checkInput("h8"), true);
    QCOMPARE(Validator::checkInput("z1"), false);
    QCOMPARE(Validator::checkInput("1"), false);
    QCOMPARE(Validator::checkInput("frdev"), false);
    QCOMPARE(Validator::checkInput("test"), false);
    QCOMPARE(Validator::checkInput("0"), false);
    QCOMPARE(Validator::checkInput("o"), false);
    QCOMPARE(Validator::checkInput("A1"), false);
    QCOMPARE(Validator::checkInput("a1"), true);
}

void Test_Validator::checkFigure()
{
    QCOMPARE(Validator::checkFigure("BR", false), true);
    QCOMPARE(Validator::checkFigure("BP", false), true);
    QCOMPARE(Validator::checkFigure("WR", false), false);
    QCOMPARE(Validator::checkFigure("", false), false);
    QCOMPARE(Validator::checkFigure("BQ", false), true);
    QCOMPARE(Validator::checkFigure("BP", true), false);
    QCOMPARE(Validator::checkFigure("WB", true), true);
    QCOMPARE(Validator::checkFigure("BK", true), false);
}

void Test_Validator::checkTurn()
{
    QCOMPARE(Validator::checkTurn("h1", "h2", mapBoard, false), false);
    QCOMPARE(Validator::checkTurn("g1", "h3", mapBoard, false), true);
    QCOMPARE(Validator::checkTurn("g1", "f4", mapBoard, false), false);
    QCOMPARE(Validator::checkTurn("a2", "a5", mapBoard, false), false);
    QCOMPARE(Validator::checkTurn("a2", "a4", mapBoard, false), true);
    QCOMPARE(Validator::checkTurn("a2", "a2", mapBoard, false), false);
    QCOMPARE(Validator::checkTurn("a2", "a3", mapBoard, true), false);
    QCOMPARE(Validator::checkTurn("b8", "c6", mapBoard, true), true);
    QCOMPARE(Validator::checkTurn("b8", "b6", mapBoard, true), false);
    QCOMPARE(Validator::checkTurn("h7", "h5", mapBoard, true), true);

    map tmpMap = mapBoard;
    for (auto& tmp : tmpMap) // remove all pawns
    {
        if (!tmp.second.isEmpty() && tmp.second.name[1] == 'P')
            tmp.second.clear();
    }
    QCOMPARE(Validator::checkTurn("a1", "a5", tmpMap, false, cast), true);
    QCOMPARE(Validator::checkTurn("a1", "b2", tmpMap, false, cast), false);
    QCOMPARE(Validator::checkTurn("b1", "a3", tmpMap, false, cast), true);
    QCOMPARE(Validator::checkTurn("b1", "b4", tmpMap, false, cast), false);
    QCOMPARE(Validator::checkTurn("c1", "a3", tmpMap, false, cast), true);
    QCOMPARE(Validator::checkTurn("c1", "c2", tmpMap, false, cast), false);
    QCOMPARE(Validator::checkTurn("d1", "d5", tmpMap, false, cast), true);
    QCOMPARE(Validator::checkTurn("d1", "b3", tmpMap, false, cast), true);
    QCOMPARE(Validator::checkTurn("d1", "h3", tmpMap, false, cast), false);
    QCOMPARE(Validator::checkTurn("e1", "e2", tmpMap, false, cast), true);
    QCOMPARE(Validator::checkTurn("e1", "d3", tmpMap, false, cast), false);
}

void Test_Validator::checkKingUnderAttack()
{
    map tmpMap = mapBoard;
    for (auto& tmp : tmpMap) // remove all pawns
    {
        if (!tmp.second.isEmpty() && tmp.second.name[1] == 'P')
            tmp.second.clear();
    }

    QCOMPARE(Validator::checkKingUnderAttack(tmpMap, false, "e1", "d2"), true);
    QCOMPARE(Validator::checkKingUnderAttack(tmpMap, true, "e8", "d7"), true);
    QCOMPARE(Validator::checkKingUnderAttack(tmpMap, true, "a8", "a2"), false);
    QCOMPARE(Validator::checkKingUnderAttack(tmpMap, true, "c8", "f5"), false);
    QCOMPARE(Validator::checkKingUnderAttack(tmpMap, true, "d8", "a5"), false);
    QCOMPARE(Validator::checkKingUnderAttack(tmpMap, false, "b1", "b3"), false);
    QCOMPARE(Validator::checkKingUnderAttack(tmpMap, false, "d1", "d6"), false);
    QCOMPARE(Validator::checkKingUnderAttack(tmpMap, false, "d8", "a5"), true);
}

void Test_Validator::checkCheck()
{
    Validator::fillUnderAttack(mapBoard);
    QCOMPARE(Validator::checkCheck(mapBoard), "");
    // fill rmpMap for verification
    map tmpMap = mapBoard;
    for (auto& tmp : tmpMap) // remove all pawns
    {
        if (!tmp.second.isEmpty() && tmp.second.name[1] == 'P')
            tmp.second.clear();
    }
    tmpMap["e1"].clear();
    tmpMap["d1"].clear();
    tmpMap["d3"].name = "BK";
    Validator::fillUnderAttack(tmpMap);
    QCOMPARE(Validator::checkCheck(tmpMap), "d3");
    tmpMap["d3"].clear();
    tmpMap["e1"].name = "BK";
    tmpMap["e3"].name = "BQ";
    Validator::fillUnderAttack(tmpMap);
    QCOMPARE(Validator::checkCheck(tmpMap), "e8");
}

void Test_Validator::checkMate()
{
    map tmpMap = mapBoard;
    for (auto& tmp : tmpMap) // remove all pawns
    {
        if (!tmp.second.isEmpty() && tmp.second.name[1] == 'P')
            tmp.second.clear();
    }
    tmpMap["d1"].clear();
    tmpMap["e6"].name = "BQ";
    tmpMap["f1"].clear();
    tmpMap["h5"].name = "BB";
    Validator::fillUnderAttack(tmpMap);
    QCOMPARE(Validator::checkMate(tmpMap, "e8"), true);
    tmpMap["h5"].clear();

    Validator::fillUnderAttack(tmpMap);
    QCOMPARE(Validator::checkMate(tmpMap, "e8"), false);
}

void Test_Validator::checkCastling()
{
    map tmpMap = mapBoard;
    for (auto& tmp : tmpMap) // remove all figures besides king and rock
    {
        if (!tmp.second.isEmpty() && tmp.second.name[1] != 'K' && tmp.second.name[1] != 'R')
            tmp.second.clear();
    }
    cast = { 0, 0, 0, 0, 0, 0 }; // can castling
    Validator::fillUnderAttack(tmpMap);
    QCOMPARE(Validator::checkCastling(tmpMap, "e8", "c8", cast), Castling::CORRECT);
    QCOMPARE(Validator::checkCastling(tmpMap, "e1", "g1", cast), Castling::CORRECT);
    cast = { 0, 0, 0, 0, 1, 1 }; // can't castling kings
    QCOMPARE(Validator::checkCastling(tmpMap, "e8", "c8", cast), Castling::CONTINUE);
    QCOMPARE(Validator::checkCastling(tmpMap, "e1", "g1", cast), Castling::CONTINUE);
    cast = { 1, 1, 1, 1, 0, 0 }; // can't castling bishops
    QCOMPARE(Validator::checkCastling(tmpMap, "e8", "g8", cast), Castling::NOT_CORRECT);
    QCOMPARE(Validator::checkCastling(tmpMap, "e1", "c1", cast), Castling::NOT_CORRECT);

    QCOMPARE(Validator::checkCastling(tmpMap, "e8", "e7", cast), Castling::CONTINUE);
}

void Test_Validator::checkStalemate()
{
    Validator::fillUnderAttack(mapBoard);
    QCOMPARE(Validator::checkStalemate(mapBoard, false), false);
    QCOMPARE(Validator::checkStalemate(mapBoard, true), false);
    // fill board for stalemate
    map tmpMap = mapBoard;
    for (auto& tmp : tmpMap) // remove all figures besides king and rock
    {
        if (!tmp.second.isEmpty() && tmp.second.name[1] != 'K' && tmp.second.name[1] != 'R')
            tmp.second.clear();
    }

    tmpMap["a8"].clear();
    tmpMap["h8"].clear(); // only king can move
    tmpMap["h7"].name = "BR";
    tmpMap["d6"].name = "BR";
    tmpMap["f5"].name = "BR"; // only for test add third rock
    Validator::fillUnderAttack(tmpMap);
    QCOMPARE(Validator::checkStalemate(tmpMap, true), true);
}

void Test_Validator::findKing()
{
    QCOMPARE(Validator::findKing(mapBoard), (pair<string, string> { "e1", "e8" }));
    map tmpMap = mapBoard;
    tmpMap["e1"].clear();
    QCOMPARE(Validator::findKing(tmpMap), (pair<string, string> { "e8", "" }));
    tmpMap["e8"].clear();
    QCOMPARE(Validator::findKing(tmpMap), (pair<string, string> { "", "" }));
}

void Test_Validator::convert()
{
    QCOMPARE(Validator::convert(0), 'a');
    QCOMPARE(Validator::convert(7), 'h');
    QCOMPARE(Validator::convert(-150), ' ');
    QCOMPARE(Validator::convert(8), ' ');
    QCOMPARE(Validator::convert(6), 'g');
}
