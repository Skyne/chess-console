#ifndef TEST_VALIDATOR_H
#define TEST_VALIDATOR_H
#include "validator.h"
#include <QTest>

class Test_Validator : public QObject
{
    Q_OBJECT
public:
    Test_Validator(QObject *parent = nullptr);

private:
    std::map<std::string, Figure> mapBoard;
    std::vector<bool> cast;
private slots:

    void checkOption();
    void checkInput();
    void checkFigure();
    void checkTurn();
    void checkKingUnderAttack();
    void checkCheck();
    void checkMate();
    void checkCastling();
    void checkStalemate();
    void findKing();
    void convert();
};

#endif // TEST_VALIDATOR_H
