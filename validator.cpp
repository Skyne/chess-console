#include "validator.h"
#include <math.h>
#include <algorithm>

using namespace std;

bool Validator::checkOption(const char& option)
{
    if (option == 'e' || option == 'l' || option == 'r' || option == 's')
        return true;
    return false;
}

bool Validator::checkInput(const string& turn)
{
    if (turn.size() != 2)
    {
        cout << "Incorrect input" << endl;
        return false;
    }
    if (int(turn[0]) < 97 || int(turn[0]) > 104)
    {
        cout << "Incorrect letter" << endl;
        return false;
    }
    if (int(turn[1]) < 49 || int(turn[1]) > 56)
    {
        cout << "Incorrect number" << endl;
        return false;
    }
    return true;
}

bool Validator::checkFigure(const string& figure, const bool& isWhite)
{
    if (figure == "")
    {
        cout << "It`s empty square" << endl;
        return false;
    }
    if (isWhite && figure[0] == 'W')
    {
        return true;
    }
    else if (!isWhite && figure[0] == 'B')
    {
        return true;
    }

    cout << "It`s enemy figure" << endl;
    return false;
}

bool Validator::checkTurn(const string& figurePos, const string& figureTurn,
                          std::map<std::string, Figure>& map, const bool& turnWhite,
                          const std::vector<bool>& checkCastling)
{
    Figure first, second;
    first = map[figurePos];
    second = map[figureTurn];

    if (first.name[0] == second.name[0])
        return false;

    char figure = first.name[1];
    switch (figure)
    {
    // PAWN
    case 'P': {

        char line = figurePos[0];
        int num = figurePos[1] - '0';
        if (abs(first.posX - second.posX) <= 2)
        {
            int turn = first.posX - second.posX;
            if (abs(turn) == 1) // check 1 step turn
            {
                if (turnWhite)
                {
                    if (first.posX - second.posX != 1)
                        return false;
                }
                else
                {
                    if (second.posX - first.posX != 1)
                        return false;
                }
                if (abs(first.posY - second.posY) == 1 && !second.isEmpty()) // check break enemy figure
                    return true;
                if (first.posY - second.posY == 0 && map[(line + to_string(num - turn))].isEmpty())
                    return true;
            }
            else if (first.posY == second.posY) // check 2 step turn
            {
                if (turnWhite)
                {
                    if (first.posX == 6)
                    {
                        if (map[(line + to_string(num - turn))].isEmpty()
                            && map[(line + to_string(num - turn + 1))].isEmpty())
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    if (first.posX == 1)
                    {
                        if (map[(line + to_string(num - turn))].isEmpty()
                            && map[(line + to_string(num - turn - 1))].isEmpty())
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
        }

        break;
    }
    // ROCK
    case 'R': {
        if (first.posX - second.posX != 0 && first.posY - second.posY != 0)
            return false;
        char line = figurePos[0];
        int num = figurePos[1] - '0';
        if (first.posX == second.posX && first.posY != second.posY) // check one line turn
        {
            if (first.posY > second.posY)
            {
                for (int i = second.posY + 1; i < first.posY; i++)
                {
                    char letter = convert(i);
                    if (!map[(letter + to_string(num))].isEmpty())
                        return false;
                }
                return true;
            }
            else
            {
                for (int i = first.posY + 1; i < second.posY; i++)
                {
                    char letter = convert(i);
                    if (!map[(letter + to_string(num))].isEmpty())
                        return false;
                }
                return true;
            }
        }
        else if (first.posX != second.posX && first.posY == second.posY) // check one line turn
        {
            if (first.posX > second.posX)
            {
                for (int i = second.posX + 2; i <= first.posX; i++)
                {
                    if (!map[(line + to_string(i))].isEmpty())
                        return false;
                }
                return true;
            }
            else
            {
                for (int i = first.posX + 2; i <= second.posX; i++)
                {
                    if (!map[(line + to_string(i))].isEmpty())
                        return false;
                }
                return true;
            }
        }

        break;
    }
    // KNIGHT
    case 'N': {
        if (abs(first.posX - second.posX) == 2 && abs(first.posY - second.posY) == 1)
            return true;
        else if (abs(first.posX - second.posX) == 1 && abs(first.posY - second.posY) == 2)
            return true;
        break;
    }
    // BISHOP
    case 'B': {
        if (abs(first.posX - second.posX) == abs(first.posY - second.posY))
        {
            if (first.posY > second.posY && first.posX > second.posX) // check up-left
            {
                int tmpY = first.posY - 1;
                for (int i = first.posX; i > second.posX + 1; i--)
                {
                    char letter = convert(tmpY);
                    if (!map[(letter + to_string(i))].isEmpty())
                        return false;
                    tmpY--;
                }

                return true;
            }
            else if (first.posY > second.posY && first.posX < second.posX) // check down-left
            {
                int tmpY = first.posY - 1;
                for (int i = first.posX + 2; i <= second.posX; i++)
                {
                    char letter = convert(tmpY);
                    if (!map[(letter + to_string(i))].isEmpty())
                        return false;
                    tmpY--;
                }
            }
            else if (first.posY < second.posY && first.posX > second.posX) // check up-right
            {
                int tmpY = first.posY + 1;
                for (int i = first.posX; i > second.posX + 1; i--)
                {
                    char letter = convert(tmpY);
                    if (!map[(letter + to_string(i))].isEmpty())
                        return false;
                    tmpY++;
                }
            }
            else if (first.posY < second.posY && first.posX < second.posX) // check down-right
            {
                int tmpY = first.posY + 1;
                for (int i = first.posX + 2; i <= second.posX; i++)
                {
                    char letter = convert(tmpY);
                    if (!map[(letter + to_string(i))].isEmpty())
                        return false;
                    tmpY++;
                }
            }
            return true;
        }

        break;
    }
    // QUEEN
    case 'Q': {

        char line = figurePos[0];
        int num = figurePos[1] - '0';

        if (abs(first.posX - second.posX) <= 1 && abs(first.posY - second.posY) <= 1)
            return true;
        else if (first.posX == second.posX && first.posY != second.posY) // check one line turn
        {
            if (first.posY > second.posY)
            {
                for (int i = second.posY + 1; i < first.posY; i++)
                {
                    char letter = convert(i);
                    if (!map[(letter + to_string(num))].isEmpty())
                        return false;
                }
                return true;
            }
            else
            {
                for (int i = first.posY + 1; i < second.posY; i++)
                {
                    char letter = convert(i);
                    if (!map[(letter + to_string(num))].isEmpty())
                        return false;
                }
                return true;
            }
        }
        else if (first.posX != second.posX && first.posY == second.posY) // check one line turn
        {
            if (first.posX > second.posX)
            {
                for (int i = second.posX + 2; i <= first.posX; i++)
                {
                    if (!map[(line + to_string(i))].isEmpty())
                        return false;
                }
                return true;
            }
            else
            {
                for (int i = first.posX + 2; i <= second.posX; i++)
                {
                    if (!map[(line + to_string(i))].isEmpty())
                        return false;
                }
                return true;
            }
        }
        else if (abs(first.posX - second.posX) == abs(first.posY - second.posY)) // check diagonal turn
        {
            if (first.posY > second.posY && first.posX > second.posX) // check up-left
            {
                int tmpY = first.posY - 1;
                for (int i = first.posX; i > second.posX + 1; i--)
                {
                    char letter = convert(tmpY);
                    if (!map[(letter + to_string(i))].isEmpty())
                        return false;
                    tmpY--;
                }

                return true;
            }
            else if (first.posY > second.posY && first.posX < second.posX) // check down-left
            {
                int tmpY = first.posY - 1;
                for (int i = first.posX + 2; i <= second.posX; i++)
                {
                    char letter = convert(tmpY);
                    if (!map[(letter + to_string(i))].isEmpty())
                        return false;
                    tmpY--;
                }
            }
            else if (first.posY < second.posY && first.posX > second.posX) // check up-right
            {
                int tmpY = first.posY + 1;
                for (int i = first.posX; i > second.posX + 1; i--)
                {
                    char letter = convert(tmpY);
                    if (!map[(letter + to_string(i))].isEmpty())
                        return false;
                    tmpY++;
                }
            }
            else if (first.posY < second.posY && first.posX < second.posX) // check down-right
            {
                int tmpY = first.posY + 1;
                for (int i = first.posX + 2; i <= second.posX; i++)
                {
                    char letter = convert(tmpY);
                    if (!map[(letter + to_string(i))].isEmpty())
                        return false;
                    tmpY++;
                }
            }
            return true;
        }
        break;
    }
    // KING
    case 'K': {
        if (abs(first.posX - second.posX) <= 1 && abs(first.posY - second.posY) <= 1)
            return true;
        else if (checkCastling.size() != 0 && abs(first.posY - second.posY) == 2
                 && first.posX - second.posX == 0) // castling check
        {
            int side = (turnWhite ? 5 : 4);
            if (first.name == "BK" && !checkCastling[side] && figurePos == "e1")
            {
                if (figureTurn == "c1" || figureTurn == "g1")
                    return true;
            }
            else if (first.name == "WK" && !checkCastling[side] && figurePos == "e8")
            {
                if (figureTurn == "c8" || figureTurn == "g8")
                    return true;
            }
        }
        break;
    }
    default: {
        break;
    }
    }
    return false;
}

bool Validator::checkKingUnderAttack(std::map<string, Figure>& maps, const bool& turnWhite,
                                     const std::string& figure, const std::string& turn)
{
    map<string, Figure> checkMap = maps;
    string tmp = checkMap[figure].name;
    checkMap[figure].clear();
    checkMap[turn].name = tmp;
    Validator::fillUnderAttack(checkMap);
    pair kingsPos = findKing(checkMap);
    string kingPos = "";
    if (turnWhite)
    {
        if (checkMap[kingsPos.first].name[0] == 'W')
            kingPos = kingsPos.first;
        else
            kingPos = kingsPos.second;
    }
    else
    {
        if (checkMap[kingsPos.first].name[0] == 'B')
            kingPos = kingsPos.first;
        else
            kingPos = kingsPos.second;
    }
    list<string> listUndAttack = checkMap[kingPos].underAttack;
    if (listUndAttack.size() != 0)
        return true;
    return false;
}

string Validator::checkCheck(std::map<std::string, Figure>& map)
{
    pair posKings = findKing(map);

    string firstKing = posKings.first;
    string secondKing = posKings.second;

    if (map[firstKing].underAttack.size() != 0)
        return firstKing;
    else if (map[secondKing].underAttack.size() != 0)
        return secondKing;

    return "";
}

bool Validator::checkMate(std::map<string, Figure>& maps, const string& kingPos)
{
    Figure king = maps[kingPos];
    char enemy = 'W';
    if (king.name[0] == 'W')
        enemy = 'B';
    for (auto& tmp : maps) // Can King move out of mate?
    {
        list<string> listUndAttack = tmp.second.underAttack;
        if (find(listUndAttack.begin(), listUndAttack.end(), kingPos) == listUndAttack.end())
            continue;

        if (any_of(listUndAttack.begin(), listUndAttack.end(),
                   [&maps, &enemy](const string& pos) { return maps[pos].name[0] == enemy; }))
            continue;
        else
        {
            bool turnWhite = false;
            if (enemy != 'W')
                turnWhite = true;
            if (!Validator::checkKingUnderAttack(maps, turnWhite, kingPos, tmp.first))
                return false;
        }
    }

    if (king.underAttack.size() > 1) // check more then 1 enemy
        return true;

    string enemyPos = king.underAttack.back();
    list checkEnemyKill = maps[enemyPos].underAttack; // Can take the attacker?
    if (any_of(checkEnemyKill.begin(), checkEnemyKill.end(), [&maps, &enemy, &enemyPos](const string& pos) {
            if (maps[pos].name[0] != enemy)
            {
                bool turnWhite = false;
                if (enemy != 'W')
                    turnWhite = true;
                if (!Validator::checkKingUnderAttack(maps, turnWhite, pos, enemyPos))
                    return true;
            }
            return false;
        }))
    {
        return false;
    }

    Figure enemyFigure = maps[enemyPos]; // Can block mate?
    int firstX = king.posX;
    int firstY = king.posY;
    int secondX = enemyFigure.posX;
    int secondY = enemyFigure.posY;
    char line = kingPos[0];
    int num = kingPos[1] - '0';
    if (firstX > secondX && firstY > secondY)
    {
        int tmpY = firstY - 1;
        for (int i = firstX; i > secondX + 1; i--)
        {
            char letter = convert(tmpY);
            list check = maps[(letter + to_string(i))].underAttack;
            if (any_of(check.begin(), check.end(), [&maps, &enemy](const string& pos) {
                    if (maps[pos].name[0] != enemy)
                        return maps[pos].name[1] != 'K';
                    return false;
                }))
            {
                return false;
            }
            tmpY--;
        }

        return true;
    }
    else if (firstX < secondX && firstY > secondY)
    {
        int tmpY = firstY - 1;
        for (int i = firstX + 2; i <= secondX; i++)
        {
            char letter = convert(tmpY);
            list check = maps[(letter + to_string(i))].underAttack;
            if (any_of(check.begin(), check.end(), [&maps, &enemy](const string& pos) {
                    if (maps[pos].name[0] != enemy)
                        return maps[pos].name[1] != 'K';
                    return false;
                }))
            {
                return false;
            }
            tmpY--;
        }
    }
    else if (firstX > secondX && firstY < secondY)
    {
        int tmpY = firstY + 1;
        for (int i = firstX; i > secondX + 1; i--)
        {
            char letter = convert(tmpY);
            list check = maps[(letter + to_string(i))].underAttack;
            if (any_of(check.begin(), check.end(), [&maps, &enemy](const string& pos) {
                    if (maps[pos].name[0] != enemy)
                        return maps[pos].name[1] != 'K';
                    return false;
                }))
            {
                return false;
            }
            tmpY++;
        }
    }
    else if (firstX < secondX && firstY < secondY)
    {
        int tmpY = firstX + 1;
        for (int i = firstX + 2; i <= secondX; i++)
        {
            char letter = convert(tmpY);
            list check = maps[(letter + to_string(i))].underAttack;
            if (any_of(check.begin(), check.end(), [&maps, &enemy](const string& pos) {
                    if (maps[pos].name[0] != enemy)
                        return maps[pos].name[1] != 'K';
                    return false;
                }))
            {
                return false;
            }
            tmpY++;
        }
    }
    else if (firstX == secondX && firstY != secondY)
    {
        if (firstY > secondY)
        {
            for (int i = secondY + 1; i <= firstY; i++)
            {
                char letter = convert(i);
                list check = maps[(letter + to_string(num))].underAttack;
                if (any_of(check.begin(), check.end(), [&maps, &enemy](const string& pos) {
                        if (maps[pos].name[0] != enemy)
                            return maps[pos].name[1] != 'K';
                        return false;
                    }))
                {
                    return false;
                }
            }
        }
        else
        {
            for (int i = firstY + 1; i <= secondY; i++)
            {
                char letter = convert(i);
                list check = maps[(letter + to_string(num))].underAttack;
                if (any_of(check.begin(), check.end(), [&maps, &enemy](const string& pos) {
                        if (maps[pos].name[0] != enemy)
                            return maps[pos].name[1] != 'K';
                        return false;
                    }))
                {
                    return false;
                }
            }
        }
    }
    else if (firstX != secondX && firstY == secondY)
    {
        if (firstX > secondX)
        {
            for (int i = secondX + 2; i <= firstX; i++)
            {
                list check = maps[(line + to_string(i))].underAttack;
                if (any_of(check.begin(), check.end(), [&maps, &enemy](const string& pos) {
                        if (maps[pos].name[0] != enemy)
                            return maps[pos].name[1] != 'K';
                        return false;
                    }))
                {
                    return false;
                }
            }
            return true;
        }
        else
        {
            for (int i = firstX + 2; i <= secondX; i++)
            {
                list check = maps[(line + to_string(i))].underAttack;
                if (any_of(check.begin(), check.end(), [&maps, &enemy](const string& pos) {
                        if (maps[pos].name[0] != enemy)
                            return maps[pos].name[1] != 'K';
                        return false;
                    }))
                {
                    return false;
                }
            }
            return true;
        }
    }
    return true;
}

std::pair<string, string> Validator::findKing(std::map<string, Figure>& map)
{
    pair<string, string> posKings = {};
    for (auto& tmp : map)
    {
        if (tmp.second.isEmpty())
            continue;
        if (tmp.second.name[1] == 'K')
        {
            if (posKings.first == "")
                posKings = { tmp.first, "" };
            else
            {
                posKings = { posKings.first, tmp.first };
                break;
            }
        }
    }
    return posKings;
}

Castling Validator::checkCastling(std::map<std::string, Figure>& maps, const string& figurePos,
                                  const string& turn, std::vector<bool>& checkCastling)
{
    if (maps[figurePos].name[1] == 'K')
    {
        if (figurePos == "e1" && !checkCastling[4])
        {
            if (turn == "c1")
            {
                if (checkCastling[0] || maps[figurePos].underAttack.size() != 0)
                    return Castling::NOT_CORRECT;
                checkCastling[4] = true;
                checkCastling[0] = true;
                return Castling::CORRECT;
            }
            else if (turn == "g1")
            {
                if (checkCastling[1] || maps[figurePos].underAttack.size() != 0)
                    return Castling::NOT_CORRECT;
                checkCastling[4] = true;
                checkCastling[1] = true;
                return Castling::CORRECT;
            }
            else
            {
                checkCastling[4] = true;
                checkCastling[0] = true;
                checkCastling[1] = true;
            }
        }

        if (figurePos == "e8" && !checkCastling[5])
        {
            if (turn == "c8")
            {
                if (checkCastling[2] || maps[figurePos].underAttack.size() != 0)
                    return Castling::NOT_CORRECT;
                checkCastling[5] = true;
                checkCastling[2] = true;
                return Castling::CORRECT;
            }
            else if (turn == "g8")
            {
                if (checkCastling[3] || maps[figurePos].underAttack.size() != 0)
                    return Castling::NOT_CORRECT;
                checkCastling[5] = true;
                checkCastling[3] = true;
                return Castling::CORRECT;
            }
            else
            {
                checkCastling[5] = true;
                checkCastling[2] = true;
                checkCastling[3] = true;
            }
        }
    }
    if (figurePos == "a1")
        checkCastling[0] = true;
    else if (figurePos == "h1")
        checkCastling[1] = true;
    else if (figurePos == "a8")
        checkCastling[2] = true;
    else if (figurePos == "h8")
        checkCastling[3] = true;

    return Castling::CONTINUE;
}

bool Validator::checkStalemate(std::map<std::string, Figure>& maps, const bool& side)
{
    char _side = (side ? 'W' : 'B');
    pair kings = Validator::findKing(maps);
    string king = kings.first;
    if (king[0] != _side)
        king = kings.second;
    for (auto& tmp : maps)
    {
        list underAtt = tmp.second.underAttack;
        if (any_of(underAtt.begin(), underAtt.end(), [&maps, &_side, &side, &king, &tmp](const string& pos) {
                if (maps[pos].name[0] == _side)
                {
                    if (maps[pos].name[1] == 'K')
                    {
                        if (!Validator::checkKingUnderAttack(maps, side, king, tmp.first))
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return false;
            }))
        {
            return false;
        }
    }
    return true;
}

void Validator::fillUnderAttack(std::map<string, Figure>& maps)
{
    for (auto& tmp : maps)
        tmp.second.underAttack.clear();
    for (auto& tmp : maps)
    {
        if (tmp.second.isEmpty())
            continue;
        string pos = tmp.first;
        bool isWhite = false;
        if (tmp.second.name[0] == 'W')
            isWhite = true;
        for (int i = 0; i <= 7; i++)
        {
            char line = Validator::convert(i);
            for (int j = 1; j <= 8; j++)
            {
                string tmpPos = line + to_string(j);
                if (Validator::checkTurn(pos, tmpPos, maps, isWhite))
                    maps[tmpPos].underAttack.push_back(tmp.first);
            }
        }
    }
}

char Validator::convert(const int& i)
{
    switch (i)
    {
    case 0:
        return 'a';
    case 1:
        return 'b';
    case 2:
        return 'c';
    case 3:
        return 'd';
    case 4:
        return 'e';
    case 5:
        return 'f';
    case 6:
        return 'g';
    case 7:
        return 'h';
    default:
        return ' ';
    }
}
