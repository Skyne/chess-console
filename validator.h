#ifndef VALIDATOR_H
#define VALIDATOR_H

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "figure.h"

enum class Castling
{
    CORRECT,
    NOT_CORRECT,
    CONTINUE
};

class Validator
{
public:
    static bool checkOption(const char &option);                             // check save, restart etc..
    static bool checkInput(const std::string &turn);                         // check correct input
    static bool checkFigure(const std::string &figure, const bool &isWhite); // check correct choose figure
    static bool checkTurn(const std::string &figurePos, const std::string &figureTurn,
                          std::map<std::string, Figure> &map, const bool &turnWhite,
                          const std::vector<bool> &checkCastling = {}); // check correct turn
    static bool checkKingUnderAttack(std::map<std::string, Figure> &map, const bool &turnWhite,
                                     const std::string &figure, const std::string &turn);
    static std::string checkCheck(std::map<std::string, Figure> &map);
    static bool checkMate(std::map<std::string, Figure> &maps, const std::string &kingPos);
    static Castling checkCastling(std::map<std::string, Figure> &maps, const std::string &figurePos,
                                  const std::string &turn, std::vector<bool> &checkCastling);
    static bool checkStalemate(std::map<std::string, Figure> &maps, const bool &side);

    static std::pair<std::string, std::string> findKing(std::map<std::string, Figure> &map);
    static void fillUnderAttack(std::map<std::string, Figure> &maps); // fill mapBoard squares
    static char convert(const int &i);                                // convert int to board chess letter
};

#endif // VALIDATOR_H
